import xml.etree.ElementTree as ET

class ItemClass:
    def __init__(self, name, amount):
        tree = ET.parse('items.xml')
        root = tree.getroot()
        for item in root.findall('item'):
            if name == item.get('name'):
                self.value = int(item.find('value').text)
                self.sell_value = int(round(self.value * 0.75))
                self.rarity = item.find('rarity').text
                self.description = item.find('description').text
                self.type = item.find('type').text
                self.name = name
                self.amount = amount

def getItemsWithRarity(rarity):
    tree = ET.parse('items.xml')
    root = tree.getroot()
    rarity_list = []
    for item in root.findall('item'):
        if rarity == item.find('rarity').text:
            rarity_list.append(item.get('name'))
    return rarity_list

def getItem(item_name):
    item = ItemClass(item_name, 1)
    return item

def getItemsWithType(type):
    tree = ET.parse('items.xml')
    root = tree.getroot()
    type_list = []
    for item in root.findall('item'):
        if type == item.find('type').text:
            type_list.append(item.get('name'))
    return type_list

def inventory_getItemsWithType(type, inventory):
    list = []
    for item in inventory:
        if item.type == type:
            list.append(item)
    return list
