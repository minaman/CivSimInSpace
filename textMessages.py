def textMessage_OutOfFuel():
    message_list = []
    message_list.append("You have run out of fuel!")
    message_list.append(" ")
    message_list.append("You drift aimlessly through the stars until a local vessel spots you.")
    message_list.append(" ")
    message_list.append("In exchange for your inventory, they tow you to their destination.")
    message_list.append("Upon arrival, they fill up half your fuel tank and bid you farewell.")
    message_list.append(" ")
    message_list.append("[Press any key to continue...]")
    return message_list

def textMessage_buyFuel(credits):
    message_list = ["Fuel Packs for Sale:", " ", "Small Fuel Pack", "Medium Fuel Pack", "Large Fuel Pack", "Huge Fuel Pack", " "]
    message_list.append("Your credits: " + str(credits))
    return message_list

def textMessage_shipInformation(player):
    vacant_spaces = player.ship.crew_capacity - 1
    message_list = []
    message_list.append(player.name + "'s ship:")
    message_list.append(" ")
    message_list.append(player.ship.name)
    message_list.append(" ")
    message_list.append("Hull: " + str(player.ship.hull) + "/" + str(player.ship.max_hull))
    message_list.append("Shield: " + str(player.ship.shield) + "/" + str(player.ship.max_shield))
    message_list.append("Fuel: " + str(player.ship.fuel) + "/" + str(player.ship.max_fuel))
    message_list.append(" ")
    message_list.append("Crew " + str(len(player.crew)) + "/" + str(player.ship.crew_capacity))
    message_list.append(" ")
    for member in player.crew:
        message_list.append(str(member.name) + ", " + str(member.profession))
        vacant_spaces = vacant_spaces - 1
    if len(player.crew) == 0:
        message_list.append("<NONE>")
    # for i in range(0, vacant_spaces):
    #     message_list.append("<VACANT>")
    message_list.append(" ")
    message_list.append("Press [ESC] to return...")
    return message_list

def textMessage_displayInventory(credits, inventory):
    message_list = ["Credits: " + str(credits), " ", "Current inventory:", " "]
    # Sorting inventory by name
    inventory.sort(key=lambda x: x.name)
    if len(inventory) == 0: # If inventory is empty
        message_list.append("NONE")
        empty = True
    else:
        empty = False
        for item in inventory:
            message_list.append(item.name + " x" + str(item.amount))
    message_list.append(" ")
    message_list.append("Press [ESC] to return")
    return message_list, empty

def textMessage_displaySmeltingInventory(inventory):
    message_list = ["Current inventory:", " ", 'Select item for smelting', ' ']
    inventory.sort(key=lambda x: x.name)
    empty = True
    for item in inventory:
        if item.type == "Ore":
            empty = False
            message_list.append(item.name + " x" + str(item.amount))
    if empty:
        message_list.append("NONE")
    return message_list, empty

def textMessage_notEnoughFuelForScan():
    message_list = []
    message_list.append("You do not have enough fuel to perform a scan!")
    message_list.append(" ")
    message_list.append("[Press any key to continue...]")
    return message_list