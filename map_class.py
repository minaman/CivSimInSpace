import random
import names #Random name generation

planet_types = ['Forge World', 'Hive World', 'Mining World', 'Desolate World', 'Government World', 'Fleet World', 'Trading Outpost']
faction_types = ['Imperium', 'Emporium', 'Cartels', 'Neutral']
#planet_types = ['Hive City', 'Forge World', 'Gladiator World', 'Pirate World', 'Desolate World', 'Government World']

class MapClass:
    def __init__(self, x, y, symbol):
        self.x = x
        self.y = y
        self.symbol = symbol
        self.scanned = False
        #self.loaded = True
        #If its a star
        if symbol == "*":
            self.name = names.get_last_name()
            self.planets = []
            self.population = 0
            self.faction = random.choice(faction_types)
            #Assigning planet information
            for i in range(0, random.randint(1, 7)):
                self.planets.append(PlanetClass())
                self.population = self.population + int(self.planets[i].population[:-1])
        #If it is empty space
        else:
            self.name = " "
            self.planets = []
            self.population = 0
            self.faction = " "

class PlanetClass:
    def __init__(self):
        self.name = names.get_last_name()
        self.planet_type = random.choice(planet_types)
        self.population = str(random.randint(1, 200)) + "m"
        self.image = None
