import libtcodpy as libtcod  # libtcod library
import random

def createPlanet(planetType):
    pix = libtcod.image_new(7, 7)
    if planetType == 'Forge World':
        baseColour = libtcod.dark_grey
        flavourColour = libtcod.dark_orange
    elif planetType == 'Desolate World':
        baseColour = libtcod.dark_yellow
        flavourColour = libtcod.yellow
    elif planetType == 'Mining World':
        baseColour = libtcod.gray
        flavourColour = libtcod.dark_gray
    elif planetType == 'Hive World':
        baseColour = libtcod.yellow
        flavourColour = libtcod.dark_gray
    elif planetType == 'Fleet World':
        baseColour = libtcod.darkest_gray
        flavourColour = libtcod.light_blue
    elif planetType == 'Trading Outpost':
        baseColour = libtcod.green
        flavourColour = libtcod.dark_amber
    else:
        baseColour = libtcod.blue
        flavourColour = libtcod.green
    for i in range(0, 7):
        for j in range(0, 7):
            libtcod.image_put_pixel(pix, i, j, baseColour)
            random_flavour = random.randint(0, 3)
            if random_flavour == 0:
                libtcod.image_put_pixel(pix, i, j, flavourColour)
    # Creating the black areas around the planet
    libtcod.image_put_pixel(pix, 0, 0, libtcod.black)
    libtcod.image_put_pixel(pix, 0, 1, libtcod.black)
    libtcod.image_put_pixel(pix, 1, 0, libtcod.black)
    libtcod.image_put_pixel(pix, 0, 6, libtcod.black)
    libtcod.image_put_pixel(pix, 0, 5, libtcod.black)
    libtcod.image_put_pixel(pix, 1, 6, libtcod.black)
    libtcod.image_put_pixel(pix, 6, 0, libtcod.black)
    libtcod.image_put_pixel(pix, 5, 0, libtcod.black)
    libtcod.image_put_pixel(pix, 6, 1, libtcod.black)
    libtcod.image_put_pixel(pix, 6, 6, libtcod.black)
    libtcod.image_put_pixel(pix, 6, 5, libtcod.black)
    libtcod.image_put_pixel(pix, 5, 6, libtcod.black)
    return pix

def selectPlanet():
    pix = libtcod.image_new(7, 7)
    for i in range(0, 7):
        for j in range(0, 7):
            libtcod.image_put_pixel(pix, i, j, libtcod.lightest_pink)
    libtcod.image_set_key_color(pix, libtcod.lightest_pink)
    libtcod.image_put_pixel(pix, 1, 1, libtcod.white)
    libtcod.image_put_pixel(pix, 2, 0, libtcod.white)
    libtcod.image_put_pixel(pix, 0, 2, libtcod.white)
    libtcod.image_put_pixel(pix, 3, 0, libtcod.white)
    libtcod.image_put_pixel(pix, 4, 0, libtcod.white)
    libtcod.image_put_pixel(pix, 0, 3, libtcod.white)
    libtcod.image_put_pixel(pix, 0, 4, libtcod.white)
    libtcod.image_put_pixel(pix, 1, 5, libtcod.white)
    libtcod.image_put_pixel(pix, 2, 6, libtcod.white)
    libtcod.image_put_pixel(pix, 3, 6, libtcod.white)
    libtcod.image_put_pixel(pix, 4, 6, libtcod.white)
    libtcod.image_put_pixel(pix, 5, 5, libtcod.white)
    libtcod.image_put_pixel(pix, 6, 4, libtcod.white)
    libtcod.image_put_pixel(pix, 6, 3, libtcod.white)
    libtcod.image_put_pixel(pix, 6, 2, libtcod.white)
    libtcod.image_put_pixel(pix, 5, 1, libtcod.white)
    return pix