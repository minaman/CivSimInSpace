from items import ItemClass
import xml.etree.ElementTree as ET
import random

profession_types = ['Navigator', 'Sapper', 'Medic', 'Surgeon', 'Mechanic']

class PlayerClass:
    def __init__(self, name):
        self.name = name
        self.credits = 50
        self.ship = ShipClass("Military Frigate")
        self.inventory = []
        self.crew = []

    def add_item(self, name, amount):
        found = False
        for item in self.inventory:
            if name == item.name:
                found = True
                item.amount += amount
        if found is False:
            self.inventory.append(ItemClass(name, amount))

    def remove_item(self, name, amount):
        for item in self.inventory:
            if name == item.name:
                if amount == -1:
                    self.inventory.remove(item)
                else:
                    item.amount -= amount
                    if item.amount <= 0:
                        self.inventory.remove(item)

    def get_amount(self, name):
        for item in self.inventory:
            if name == item.name:
                return item.amount
        return 0

    def add_crew(self, name, age, sex):
        if len(self.crew) < self.ship.crew_capacity - 1:
            self.crew.append(CrewClass(name, age, sex, random.choice(profession_types)))

class ShipClass:
    def __init__(self, name):
        tree = ET.parse('ships.xml')
        root = tree.getroot()
        for ship in root.findall('ship'):
            if name == ship.get('name'):
                self.name = name
                self.max_hull = int(ship.find('maxhull').text)
                self.max_shield = int(ship.find('maxshield').text)
                self.max_fuel = int(ship.find('maxfuel').text)
                self.hull = self.max_hull
                self.shield = self.max_shield
                self.fuel = self.max_fuel
                self.inventory_capacity = int(ship.find('inventorycapacity').text)
                self.crew_capacity = int(ship.find('crewcapacity').text)

def getAllShips():
    tree = ET.parse('ships.xml')
    root = tree.getroot()
    ship_list = []
    for ship in root.findall('ship'):
        ship_to_add = ShipClass(ship.get('name'))
        ship_list.append(ship_to_add)
    return ship_list

class CrewClass:
    def __init__(self, name, age, sex, profession):
        self.name = name
        self.sex = sex
        self.age = age
        self.profession = profession