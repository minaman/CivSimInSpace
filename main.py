from map_class import MapClass #Used to hold data about the map and planets
from actor_class import PlayerClass, ShipClass, ItemClass, getAllShips
from planet_images import createPlanet, selectPlanet #Library functions to create planet images
from items import * #Function to perform on items.xml
from textMessages import * #Used to display lengthy messages

import time #Used for delays
import libtcodpy as libtcod  # libtcod library
import random  # Random number generation
import textwrap  # Used to turn a long strong into separate lines
import names

##Playing of music
import pygame
pygame.init()
pygame.mixer.music.load('sunshine.mp3')
pygame.mixer.music.play(0)
pygame.mixer.music.set_pos(40.0)

# # GLOBAL VARIABLES
# actual size of the window
SCREEN_WIDTH = 120
SCREEN_HEIGHT = 67

# Boundaries from centre point to edge of screen
X_Bound = int(((SCREEN_WIDTH - 50) / 2))
Y_Bound = int((SCREEN_HEIGHT - 3) / 2)

# Size of the fuel, shield, health panel
panel = libtcod.console_new(SCREEN_WIDTH, 3)

# Size of the message log
MSG_WIDTH = 25
MSG_HEIGHT = SCREEN_HEIGHT - 3

# create the list of game messages and their colors, starts empty
game_msgs = []

#Map details
Map_X = 200
Map_Y = 200
Map = {}

# Player initial values
playerx = int(Map_X / 2)
playery = int(Map_Y / 2)
playersymbol = chr(30)

#Creation of initial player class
player = PlayerClass("Alex")

#Variables used to store navigation through inventory and planets
planet_num = 0
inv_num = 0
smelting_inv_num = 0
smelting_inv_max_num = 0
crew_num = 0
fleet_world_fuel_num = 0
fleet_world_buy_ship_num = 0
trading_outpost_buy_num = 0
trading_outpost_buy_max_num = 0
trading_outpost_sell_num = 0

#Variable used for item usage
item_selected = 0
number_of_items_on_sale = len(getItemsWithType("Material"))
total_ships = len(getAllShips())

#Creating a list of blank values to clear menus
blanklist = []
for i in range(0, 20):
    blanklist.append('NL')

#Global variables to say which menu is currently on
main_menu = True
planets_display = False
inventory_menu = False
ship_menu = False
smelting_menu = False
fleet_world_fuel_menu = False
fleet_world_buy_ship_menu = False
trading_outpost_buy_menu = False
trading_outpost_sell_menu = False

def message(new_msg, color=libtcod.white):
    # split the message if necessary, among multiple lines
    new_msg_lines = textwrap.wrap(new_msg, MSG_WIDTH)
    for line in new_msg_lines:
        # if the buffer is full, remove the first line to make room for the new one
        if len(game_msgs) == MSG_HEIGHT:
            del game_msgs[0]
        # add the new line as a tuple, with the text and the color
        game_msgs.append((line, color))

def menu(header, options, width, posx, posy):
    # calculate total height for the header (after auto-wrap) and one line per option
    header_height = libtcod.console_get_height_rect(None, posx, posy, width, SCREEN_HEIGHT, header)
    if header == '':
        header_height = 0
    height = len(options) + header_height
    # create an off-screen console that represents the menu's window
    window = libtcod.console_new(width, height)
    # print the header, with auto-wrap
    libtcod.console_set_default_foreground(window, libtcod.white)
    libtcod.console_print_rect_ex(window, posx, posy, width, height, libtcod.BKGND_NONE, libtcod.LEFT, header)
    # print all the options
    y = header_height
    letter_index = 1
    for option_text in options:
        if option_text == 'NL': #If you want a blank line
            text = '                    '
        elif option_text[0] == '#': #If you just want a custom message, start it with a #
            text = option_text[1:]
        else: #Otherwise print it with the number in front
            text = '(' + str(letter_index) + ') ' + option_text
        libtcod.console_print_ex(window, 0, y, libtcod.BKGND_NONE, libtcod.LEFT, text)
        y += 1
        letter_index += 1
    libtcod.console_blit(window, posx, posy, width, height, 0, 97, 0, 1.0, 0.7)

def render_bar(panel, x, y, total_width, name, value, maximum, bar_color, back_color):
    # render a bar (HP, experience, etc). first calculate the width of the bar
    bar_width = int(float(value) / maximum * total_width)
    # render the background first
    libtcod.console_set_default_background(panel, back_color)
    libtcod.console_rect(panel, x, y, total_width, 1, False, libtcod.BKGND_SCREEN)
    # now render the bar on top
    libtcod.console_set_default_background(panel, bar_color)
    if bar_width > 0:
        libtcod.console_rect(panel, x, y, bar_width, 1, False, libtcod.BKGND_SCREEN)
    # finally, some centered text with the values
    libtcod.console_set_default_foreground(panel, libtcod.white)
    libtcod.console_print_ex(panel, x + int(total_width / 2), y, libtcod.BKGND_NONE, libtcod.CENTER, name + ': ' + str(value) + '/' + str(maximum))

def handle_keys():
    global playerx, playery, playersymbol
    g = globals()
    just_loaded = False
    key = libtcod.console_wait_for_keypress(True)  # turn-based
    ##INVENTORY MENU
    if g["inventory_menu"]:
        if key.vk == libtcod.KEY_ESCAPE:  # If quitting inventory display
            g["inventory_menu"] = False
            g["inv_num"] = 0
            return True, 0
        elif key.c == ord('s') and (g["inv_num"] + 1 < len(player.inventory)):
            g["inv_num"] += 1
        elif key.c == ord('w') and g["inv_num"] > 0:
            g["inv_num"] -= 1
        return False, g["inv_num"]
    ##SHIP MENU
    if g["ship_menu"]:
        if key.vk == libtcod.KEY_ESCAPE:  # If quitting ship display
            g["ship_menu"] = False
            g["crew_num"] = 0
            return True, 0
        elif key.c == ord('s') and (g["crew_num"] + 1 < len(player.crew)):
            g["crew_num"] += 1
        elif key.c == ord('w') and g["crew_num"] > 0:
            g["crew_num"] -= 1
        return False, g["crew_num"]
    ## TRADING OUTPOST BUY
    if g["trading_outpost_buy_menu"]:
        if key.vk == libtcod.KEY_ESCAPE:  # If quitting inventory display
            g["trading_outpost_buy_menu"] = False
            g["trading_outpost_buy_num"] = 0
            return True, 0
        elif key.c == ord('s') and (g["trading_outpost_buy_num"] + 1 < g["number_of_items_on_sale"] - 1):
            g["trading_outpost_buy_num"] += 1
        elif key.c == ord('w') and g["trading_outpost_buy_num"] > 0:
            g["trading_outpost_buy_num"] -= 1
        elif key.c == 49:
            buyItem(g["trading_outpost_buy_num"])
        return False, g["trading_outpost_buy_num"]
    ## TRADING OUTPOST SELL
    if g["trading_outpost_sell_menu"]:
        if key.vk == libtcod.KEY_ESCAPE:  # If quitting inventory display
            g["trading_outpost_sell_menu"] = False
            g["trading_outpost_sell_num"] = 0
            return True, 0
        elif key.c == ord('s') and (g["trading_outpost_sell_num"] + 1 < len(player.inventory)):
            g["trading_outpost_sell_num"] += 1
        elif key.c == ord('w') and g["trading_outpost_sell_num"] > 0:
            g["trading_outpost_sell_num"] -= 1
        elif key.c == 49 and len(player.inventory) > 0: #Ree
            sellItem()
        return False, g["trading_outpost_sell_num"]
    ## FLEET WORLD BUY FUEL MENU
    if g["fleet_world_fuel_menu"]:
        if key.vk == libtcod.KEY_ESCAPE:  # If quitting inventory display
            g["fleet_world_fuel_menu"] = False
            g["fleet_world_fuel_num"] = 0
            return True, 0
        elif key.c == ord('s') and (g["fleet_world_fuel_num"] + 1 < 4):
            g["fleet_world_fuel_num"] += 1
        elif key.c == ord('w') and g["fleet_world_fuel_num"] > 0:
            g["fleet_world_fuel_num"] -= 1
        elif key.c == 49:
            purchaseFuel(g["fleet_world_fuel_num"])
        return False, g["fleet_world_fuel_num"]
    ## FLEET WORLD BUY SHIP MENU
    if g["fleet_world_buy_ship_menu"]:
        if key.vk == libtcod.KEY_ESCAPE:  # If quitting inventory display
            g["fleet_world_buy_ship_menu"] = False
            g["fleet_world_buy_ship_num"] = 0
            return True, 0
        elif key.c == ord('s') and (g["fleet_world_buy_ship_num"] + 1 < g["total_ships"]):
            g["fleet_world_buy_ship_num"] += 1
        elif key.c == ord('w') and g["fleet_world_buy_ship_num"] > 0:
            g["fleet_world_buy_ship_num"] -= 1
        return False, g["fleet_world_buy_ship_num"]
    ##SMELTING MENU
    if g["smelting_menu"]:
        if key.vk == libtcod.KEY_ESCAPE:  # If quitting inventory display
            g["smelting_menu"] = False
            g["smelting_inv_num"] = 0
            return True, 0
        elif key.c == ord('s') and (g["smelting_inv_num"] + 1 < g["smelting_inv_max_num"]):
            g["smelting_inv_num"] += 1
        elif key.c == ord('w') and g["smelting_inv_num"] > 0:
            g["smelting_inv_num"] -= 1
        elif key.c == 49: # 1
            smeltItem()
        return False, g["smelting_inv_num"]
    ##PLANETS DISPLAY
    if g["planets_display"]:
        if key.vk == libtcod.KEY_ESCAPE: #If quitting planet display
            g["planets_display"] = False
            return True, 0
        #Navigation of planets
        elif key.c == ord('d') and (g["planet_num"] + 1 < len(Map[playerx, playery].planets)):
            g["planet_num"] += 1
        elif key.c == ord('a') and g["planet_num"] > 0:
            g["planet_num"] -= 1
        elif key.c == 49 and just_loaded == False: ##IF FIRST OPTION IS PRESSED
            if Map[playerx, playery].planets[g["planet_num"]].planet_type == "Desolate World":
                scavengePlanet()
            elif Map[playerx, playery].planets[g["planet_num"]].planet_type == "Mining World":
                minePlanet()
            elif Map[playerx, playery].planets[g["planet_num"]].planet_type == "Forge World":
                g["smelting_menu"] = True
                smeltOres(g["smelting_inv_num"])
            elif Map[playerx, playery].planets[g["planet_num"]].planet_type == "Fleet World":
                g["fleet_world_fuel_menu"] = True
                buyFuel(0)
            elif Map[playerx, playery].planets[g["planet_num"]].planet_type == "Trading Outpost":
                g["trading_outpost_buy_menu"] = True
                tradingoutPost_buyItems(0)
        elif key.c == 50: ## IF SECOND OPTION IS RPESSED
            if Map[playerx, playery].planets[g["planet_num"]].planet_type == "Trading Outpost":
                g["trading_outpost_sell_menu"] = True
                tradingOutpost_sellItems(0)
            elif Map[playerx, playery].planets[g["planet_num"]].planet_type == "Fleet World":
                print("Buying ship")
                g["fleet_world_buy_ship_menu"] = True
                fleetWorld_buyShip(0)
        elif key.c == ord('i'): #Pressed i
            g["inventory_menu"] = True
            displayInventory(0)
        elif key.c == ord('u'):
            g["ship_menu"] = True
            displayShip(0)
        return False, planet_num
    ## MAIN NAVIGATION VIEW
    if g["main_menu"]:
        if key.vk == libtcod.KEY_ESCAPE:
            return True  # exit game
        if key.c == ord('w') and (playery > int((SCREEN_HEIGHT - 3) / 2) + 1):
            playery -= 1
            playersymbol = chr(30) #UP ARROW
            player.ship.fuel -= 1
            GenerateMap(playerx, playery, 0)
        elif key.c == ord('s') and (playery < int(Map_Y - ((SCREEN_HEIGHT - 3) / 2))):
            playery += 1
            playersymbol = chr(31) #DOWN ARROW
            player.ship.fuel -= 1
            GenerateMap(playerx, playery, 1)
        elif key.c == ord('a') and (playerx - int((SCREEN_WIDTH - 50) / 2) > 0):
            playerx -= 1
            playersymbol = chr(17) #LEFT ARROW
            player.ship.fuel -= 1
            GenerateMap(playerx, playery, 2)
        elif key.c == ord('d') and (playerx < int(Map_X - ((SCREEN_WIDTH - 50) / 2)) - 1):
            playerx += 1
            playersymbol = chr(16) #RIGHT ARROW
            player.ship.fuel -= 1
            GenerateMap(playerx, playery, 3)
        elif key.c == ord('f'):  # CHEAT
            player.ship.fuel += 5
            if player.ship.fuel > player.ship.max_fuel:
                player.ship.fuel = player.ship.max_fuel
        elif key.c == ord('c'):  # CHEAT
            player.add_crew(names.get_full_name(gender='male'), random.randint(18, 60), "Male")
            player.add_item("The Sword of a Thousand Truths", 1)
        elif key.c == ord('i'): # Pressed i
            g["inventory_menu"] = True
            displayInventory(0)
        elif key.c == ord('u'):
            g["ship_menu"] = True
            displayShip(0)
        elif key.c == 32 and Map[playerx, playery].symbol != " " and Map[playerx, playery].scanned == False:  # Pressed space on star (scans)
            if player.ship.fuel < 5:
                clearMapDisplay()
                message_list = textMessage_notEnoughFuelForScan()
                displayText(26, int((SCREEN_HEIGHT - 3) / 2) - len(message_list), message_list)
                libtcod.console_wait_for_keypress(True)
            else:
                Map[playerx, playery].scanned = True
                Map[playerx, playery].symbol = Map[playerx, playery].faction[0]
                player.ship.fuel -= 5
        elif key.c == 49 and Map[playerx, playery].symbol != " ": #If key is 1 and you're on a star
            g["planets_display"] = True
            g["planet_num"] = 0
            displayPlanetsMenu(len(Map[playerx, playery].planets), planet_num)
        return False

def fleetWorld_buyShip(num):
    exit = False
    while not exit:
        print("inside")
        clearScreenAndDelGameMsg()
        # Display panels at bottom
        displayPanels()
        libtcod.console_blit(panel, 0, 0, SCREEN_WIDTH, 3, 0, 0, SCREEN_HEIGHT - 3)
        # Getting all ships
        print("pretest")
        all_ships = getAllShips()
        print("test")
        print(all_ships)
        message_list = ["Ships for sale:", " "]
        # Printing the list
        for ship in all_ships:
            message_list.append(ship.name)
        message_list.append(" ")
        message_list.append("Credits: " + str(player.credits))
        message("Ship Information", libtcod.red)
        message("new_line", libtcod.red)
        message("Name  : " + all_ships[num].name)
        message("Hull  : " + str(all_ships[num].max_hull))
        message("Shield: " + str(all_ships[num].max_shield))
        message("Fuel  : " + str(all_ships[num].max_fuel))
        message("new_line")
        message("Inventory: " + str(all_ships[num].inventory_capacity))
        message("Crew Cap : " + str(player.get_amount(all_ships[num].crew_capacity)))
        message("Value    : " + str(99999))
        displayMessages()
        #Menu
        menu('Trading Outpost', ['Buy selected item', 'NL', '#Press [ESC] to return'], 24, 0, 0)
        # Place the arrow
        libtcod.console_put_char(0, 25, 3 + num, ">", libtcod.BKGND_NONE)
        #Displaying text and waiting for keypress
        displayText(26, 1, message_list)
        try:
            exit, num = handle_keys()
        except TypeError:
            exit = False

def tradingoutPost_buyItems(num):
    exit = False
    while not exit:
        clearScreenAndDelGameMsg()
        # Display panels at bottom
        displayPanels()
        libtcod.console_blit(panel, 0, 0, SCREEN_WIDTH, 3, 0, 0, SCREEN_HEIGHT - 3)
        # Getting all items of type Material
        potential_items = getItemsWithType("Material")
        found_item_list = []
        message_list = ["Items for sale:", " "]
        for i in range(0, len(potential_items) - 1):
            found_item = getItem(potential_items[i])
            found_item_list.append(found_item)
        # Sorting the list by value
        found_item_list.sort(key=lambda x: x.name)
        # Printing the list
        for item in found_item_list:
            message_list.append(item.name + ", " + str(item.value))
        message_list.append(" ")
        message_list.append("Credits: " + str(player.credits))
        message("Item Information", libtcod.red)
        message("new_line", libtcod.red)
        message("Name:   " + found_item_list[num].name)
        message("Rarity: " + found_item_list[num].rarity)
        message("Type:   " + found_item_list[num].type)
        message("new_line")
        message("Value:  " + str(found_item_list[num].value))
        message("Stored: " + str(player.get_amount(found_item_list[num].name)))
        message("new_line")
        message("Description", libtcod.red)
        message("new_line")
        message(found_item_list[num].description)
        displayMessages()
        #Menu
        menu('Trading Outpost', ['Buy selected item', 'NL', '#Press [ESC] to return'], 24, 0, 0)
        # Place the arrow
        libtcod.console_put_char(0, 25, 3 + num, ">", libtcod.BKGND_NONE)
        #Displaying text and waiting for keypress
        displayText(26, 1, message_list)
        try:
            exit, num = handle_keys()
        except TypeError:
            exit = False

def buyItem(num):
    # Getting all items of type Material
    potential_items = getItemsWithType("Material")
    found_item_list = []
    for i in range(0, len(potential_items) - 1):
        found_item = getItem(potential_items[i])
        found_item_list.append(found_item)
    # Sorting the list by value
    found_item_list.sort(key=lambda x: x.name)
    print(found_item_list[num].name)
    if found_item_list[num].value <= player.credits:
        player.add_item(found_item_list[num].name, 1)
        player.credits -= found_item_list[num].value

def tradingOutpost_sellItems(num):
    global item_selected
    exit = False
    while not exit:
        clearScreenAndDelGameMsg()
        message_list, empty = textMessage_displayInventory(player.credits, player.inventory)
        message_list = message_list[:-2]
        # Display panels at bottom
        displayPanels()
        libtcod.console_blit(panel, 0, 0, SCREEN_WIDTH, 3, 0, 0, SCREEN_HEIGHT - 3)
        menu('Trading Outpost', ['Sell selected item', 'NL', '#Press [ESC] to return'], 24, 0, 0)
        # Display item info
        if empty == False:
            message("Item Information", libtcod.red)
            message("new_line", libtcod.red)
            try:
                message("Name:   " + player.inventory[num].name)
            except IndexError:
                time.sleep(0.5)
                num -= 1
                message("Name:   " + player.inventory[num].name)
            message("Rarity: " + player.inventory[num].rarity)
            message("Type:   " + player.inventory[num].type)
            message("new_line")
            message("Amount: " + str(player.inventory[num].amount))
            message("Price : " + str(player.inventory[num].sell_value))
            message("Total:  " + str(player.inventory[num].amount * player.inventory[num].sell_value))
            message("new_line")
            message("Description", libtcod.red)
            message("new_line")
            message(player.inventory[num].description)
            displayMessages()
        item_selected = num
        # Place the arrow
        libtcod.console_put_char(0, 26, 5 + num, ">", libtcod.BKGND_NONE)
        # Display the text
        displayText(27, 1, message_list)
        # Display item info
        libtcod.console_flush()
        try:
            exit, num = handle_keys()
        except TypeError:
            exit = False

def sellItem():
    global item_selected
    player.credits += player.inventory[item_selected].sell_value
    player.remove_item(player.inventory[item_selected].name, 1)

def scavengePlanet():
    clearScreenAndDelGameMsg()
    # Scavenging text
    message_list = ["Scavenging..."]
    loading_bar = [""]
    displayText(int(SCREEN_WIDTH / 2) - 9, int(SCREEN_HEIGHT / 2) - 3, message_list)
    #Loading bar
    for i in range(0, 15):
        displayText(int(SCREEN_WIDTH / 2) - 9, int(SCREEN_HEIGHT / 2) - 2, loading_bar)
        time.sleep(0.01) #NORMALLY 0.1
        loading_bar[0] = "-" * i
    libtcod.console_clear(None)
    message_list = ["You have found:", " "]
    # Fetching items found
    potential_items = getItemsWithRarity("Poor")
    potential_items = potential_items + getItemsWithRarity("Common")
    # Total items found
    total_items = random.randint(0, 4)
    for z in range(0, total_items):
        random_amount = random.randint(1, 5)
        item_chosen = random.choice(potential_items)
        player.add_item(item_chosen, random_amount)
        message_list.append(str(random_amount) + " x " + item_chosen)
    if total_items == 0:
        message_list.append("Nothing :(")
    message_list.append(" ")
    message_list.append("[Press any key to return]")
    displayText(26, int(SCREEN_HEIGHT / 2)  - 6, message_list)
    libtcod.console_wait_for_keypress(True)

def smeltOres(smelt_inv_num):
    global smelting_inv_max_num, item_selected
    exit = False
    while not exit:
        clearScreenAndDelGameMsg()
        menu('Forge World', ['Smelt selected ore', 'NL', '#Press [ESC] to return'], 24, 0, 0)
        message_list, empty = textMessage_displaySmeltingInventory(player.inventory)
        smelting_inventory = inventory_getItemsWithType("Ore", player.inventory)
        smelting_inv_max_num = len(smelting_inventory)
        #Display panels at bottom
        displayPanels()
        libtcod.console_blit(panel, 0, 0, SCREEN_WIDTH, 3, 0, 0, SCREEN_HEIGHT - 3)
        #Display the text
        displayText(27, 1, message_list)
        #Display item info
        if empty == False:
            message("Item Information", libtcod.red)
            message("new_line", libtcod.red)
            try:
                message("Name:    " + smelting_inventory[smelt_inv_num].name)
            except IndexError:
                smelt_inv_num -= 1
                message("Name:    " + smelting_inventory[smelt_inv_num].name)
            message("Rarity:  " + smelting_inventory[smelt_inv_num].rarity)
            message("Type:    " + smelting_inventory[smelt_inv_num].type)
            message("new_line")
            message("Amount:  " + str(smelting_inventory[smelt_inv_num].amount))
            message("Value:   " + str(smelting_inventory[smelt_inv_num].value))
            message("Total:   " + str(
                smelting_inventory[smelt_inv_num].amount * smelting_inventory[smelt_inv_num].value))
            message("new_line")
            message("Description", libtcod.red)
            message("new_line")
            message(smelting_inventory[smelt_inv_num].description)
            if smelting_inventory[smelt_inv_num].type == "Ore":
                message("new_line")
                message("Smelting Information", libtcod.red)
                message("new_line")
                message("Reagents: " + smelting_inventory[smelt_inv_num].name)
                message("Produces: " + smelting_inventory[smelt_inv_num].name[:-4])
            displayMessages()
            # Place the arrow
            libtcod.console_put_char(0, 26, 5 + smelt_inv_num, ">", libtcod.BKGND_NONE)
        item_selected = smelt_inv_num
        libtcod.console_flush()
        try:
            exit, smelt_inv_num = handle_keys()
        except TypeError:
            exit = False

def smeltItem():
    global item_selected
    print(item_selected)
    smelting_inventory = inventory_getItemsWithType("Ore", player.inventory)
    if len(smelting_inventory) != 0:
        if smelting_inventory[item_selected].name == "Gold Ore":
            player.add_item("Gold", 1)
            player.remove_item("Gold Ore", 1)
        elif smelting_inventory[item_selected].name == "Silver Ore":
            player.add_item("Silver", 1)
            player.remove_item("Silver Ore", 1)

def minePlanet():
    clearScreenAndDelGameMsg()
    # Scavenging text
    message_list = ["Mining..."]
    loading_bar = [""]
    displayText(int(SCREEN_WIDTH / 2) - 9, int(SCREEN_HEIGHT / 2) - 3, message_list)
    #Loading bar
    for i in range(0, 15):
        displayText(int(SCREEN_WIDTH / 2) - 9, int(SCREEN_HEIGHT / 2) - 2, loading_bar)
        time.sleep(0.01) ##NORMALLY 0.1
        loading_bar[0] = "-" * i
    libtcod.console_clear(None)
    message_list = ["You have found:", " "]
    # Fetching items found
    potential_items = getItemsWithType("Ore")
    potential_items = potential_items + getItemsWithType("Mining")
    # Total items found
    total_items = random.randint(0, 4)
    for z in range(0, total_items):
        random_amount = random.randint(1, 5)
        item_chosen = random.choice(potential_items)
        player.add_item(item_chosen, random_amount)
        message_list.append(str(random_amount) + " x " + item_chosen)
    if total_items == 0:
        message_list.append("Nothing :(")
    message_list.append(" ")
    message_list.append("[Press any key to return]")
    displayText(26, int(SCREEN_HEIGHT / 2)  - 6, message_list)
    libtcod.console_wait_for_keypress(True)

def buyFuel(num):
    exit = False
    while not exit:
        clearScreenAndDelGameMsg()
        menu('Fleet World', ['Buy fuel pack', 'NL', '#Press [ESC] to return'], 24, 0, 0)
        message_list = textMessage_buyFuel(player.credits)
        #Display panels at bottom
        displayPanels()
        libtcod.console_blit(panel, 0, 0, SCREEN_WIDTH, 3, 0, 0, SCREEN_HEIGHT - 3)
        #Display the text
        displayText(27, 1, message_list)
        #Display item info
        if num == 0:
            name = "Small Fuel Pack" ##Gives you 5 fuel
            fuel_amount = 5 * (2 ** (num))
            value = int(0.8 * fuel_amount)
        elif num == 1:
            name = "Medium Fuel Pack" ##10 Fuel
            fuel_amount = 5 * (2 ** (num))
            value = int(0.8 * fuel_amount)
        elif num == 2:
            name = "Large Fuel Pack" ## 20 Fuel
            fuel_amount = 5 * (2 ** (num))
            value = int(0.8 * fuel_amount)
        elif num == 3:
            name = "Huge Fuel Pack" ## 40 Fuel
            fuel_amount = 5 * (2 ** (num))
            value = int(0.8 * fuel_amount)
        description = "A " + name + " that holds " + str(fuel_amount) + " fuel, and costs only " + str(value) + " credits!"
        message("Item Information", libtcod.red)
        message("new_line", libtcod.red)
        message("Name:   " + name)
        message("Value:  " + str(value))
        message("Fuel:   " + str(fuel_amount))
        message("new_line")
        message("Description", libtcod.red)
        message("new_line")
        message(description)
        displayMessages()
        # Place the arrow
        libtcod.console_put_char(0, 26, 3 + num, ">", libtcod.BKGND_NONE)
        libtcod.console_flush()
        try:
            exit, num = handle_keys()
        except TypeError:
            exit = False

def purchaseFuel(num):
    ## Geometric sequence to calculate fuel and cost
    fuel_amount = 5 * (2 ** (num))
    value = int(0.8 * fuel_amount)
    if value < player.credits and player.ship.max_fuel != player.ship.fuel:
        player.credits -= value
        player.ship.fuel += fuel_amount
        if player.ship.fuel > player.ship.max_fuel:
            player.ship.fuel = player.ship.max_fuel

def displayInventory(num):
    exit = False
    while not exit:
        clearScreenAndDelGameMsg()
        message_list, empty = textMessage_displayInventory(player.credits, player.inventory)
        #Display panels at bottom
        displayPanels()
        libtcod.console_blit(panel, 0, 0, SCREEN_WIDTH, 3, 0, 0, SCREEN_HEIGHT - 3)
        #Display the text
        displayText(27, 1, message_list)
        #Display item info
        if empty == False:
            message("Item Information", libtcod.red)
            message("new_line", libtcod.red)
            message("Name:   " + player.inventory[num].name)
            message("Rarity: " + player.inventory[num].rarity)
            message("Type:   " + player.inventory[num].type)
            message("new_line")
            message("Amount: " + str(player.inventory[num].amount))
            message("Value:  " + str(player.inventory[num].value))
            message("Total:  " + str(player.inventory[num].amount * player.inventory[num].value))
            message("new_line")
            message("Description", libtcod.red)
            message("new_line")
            message(player.inventory[num].description)
            displayMessages()
            # Place the arrow
            libtcod.console_put_char(0, 26, 5 + num, ">", libtcod.BKGND_NONE)
        libtcod.console_flush()
        try:
            exit, num = handle_keys()
        except TypeError:
            exit = False

def displayShip(crew_num):
    exit_program = False
    while not exit_program:
        clearScreenAndDelGameMsg()
        message_list = textMessage_shipInformation(player)
        # Display panels at bottom
        displayPanels()
        libtcod.console_blit(panel, 0, 0, SCREEN_WIDTH, 3, 0, 0, SCREEN_HEIGHT - 3)
        # Display the text
        displayText(27, 1, message_list)
        # menu info
        if len(player.crew) > 0:
            message("Item Information", libtcod.red)
            message("new_line", libtcod.red)
            message("Name: " + player.crew[crew_num].name)
            message("Age:  " + str(player.crew[crew_num].age))
            message("Sex:  " + player.crew[crew_num].sex)
            message("new_line")
            message("Profession Information", libtcod.red)
            message("Job:  " + player.crew[crew_num].profession)
            displayMessages()
            # Place the arrow
            libtcod.console_put_char(0, 26, 11 + crew_num, ">", libtcod.BKGND_NONE)
        libtcod.console_flush()
        try:
            exit_program, crew_num = handle_keys()
        except TypeError:
            exit_program = False

def displayMainNavigation():
    exit_program = False
    while not exit_program:
        del game_msgs[:]
        if Map[playerx, playery].symbol != " " and Map[playerx, playery].symbol != ".":
            if not Map[playerx, playery].scanned:
                menu(Map[playerx, playery].name, ['Enter Solar System', 'NL', '#Press [SPACE] to scan', 'NL', '#i. View inventory', '#u. View ship'], 24, 0, 0)
                message("Star System: " + Map[playerx, playery].name, libtcod.red)
                message("new_line", libtcod.red)
                message("<SYSTEM SCAN REQUIRED>")
            else:
                menu(Map[playerx, playery].name, ['Enter Solar System', 'NL', '#i. View inventory', '#u. View ship'], 24, 0, 0)
                message("Star System: " + Map[playerx, playery].name, libtcod.red)
                message("Population: " + str(Map[playerx, playery].population) + " million", libtcod.red)
                message("new_line", libtcod.red)
                message("Number of planets: " + str(len(Map[playerx, playery].planets)), libtcod.red)
                message("Faction: " + str(Map[playerx, playery].faction), libtcod.red)
                message("new_line", libtcod.red)
                for i in range(0, len(Map[playerx, playery].planets)):
                    message(str(i + 1) + ": " + Map[playerx, playery].planets[i].name)
                    message(str(i + 1) + ": " + Map[playerx, playery].planets[i].planet_type)
                    message(str(i + 1) + ": " + Map[playerx, playery].planets[i].population)
                    message("new_line", libtcod.red)
        else:
            menu("Empty Space", ['NL','#i. View inventory', '#u. View ship'], 24, 0, 0)
        # Display the map
        displayMap()
        # Display the messages
        displayMessages()
        # show the panels
        displayPanels()
        # blit the contents of "panel" to the root console
        libtcod.console_blit(panel, 0, 0, SCREEN_WIDTH, 3, 0, 0, SCREEN_HEIGHT - 3)
        libtcod.console_flush()
        # Clear the menu in case of menu changes
        libtcod.console_clear(None)
        try:
            # handle keys and exit game if needed
            exit_program = handle_keys()
            # Checking if fuel has run out
            if player.ship.fuel <= 0:
                message_list = textMessage_OutOfFuel()
                displayText(26, int((SCREEN_HEIGHT - 3) / 2) - len(message_list), message_list)
                time.sleep(1)
                libtcod.console_wait_for_keypress(True)
                player.ship.fuel = int(player.ship.max_fuel / 2)
        except TypeError:
            exit_program = False

def displayPlanetsMenu(num, planet_num2):
    exit = False
    while not exit:
        #Clearing the screen
        clearScreenAndDelGameMsg()
        #Putting in individual planet info
        message("Name: " + Map[playerx, playery].planets[planet_num2].name, libtcod.red)
        message("new_line", libtcod.red)
        message("Type: " + Map[playerx, playery].planets[planet_num2].planet_type)
        message("new_line", libtcod.red)
        message("Population: " + Map[playerx, playery].planets[planet_num2].population)
        #Assigning menu options depending on planet
        different_num = False
        if Map[playerx, playery].planets[planet_num2].planet_type == "Desolate World":
            menu_option = "Scavenge for loot"
        elif Map[playerx, playery].planets[planet_num2].planet_type == "Mining World":
            menu_option = "Mine planet"
        elif Map[playerx, playery].planets[planet_num2].planet_type == "Fleet World":
            menu_option1 = "Purchase fuel"
            menu_option2 = "Upgrade ship"
            different_num = True
        elif Map[playerx, playery].planets[planet_num2].planet_type == "Forge World":
            menu_option = "Smelt ores"
        elif Map[playerx, playery].planets[planet_num2].planet_type == "Trading Outpost":
            menu_option1 = "Buy items"
            menu_option2 = "Sell items"
            different_num = True
        else:
            menu_option = "Enter planet"
        #Right hand side menu
        if different_num == False:
            menu(Map[playerx, playery].planets[planet_num2].name, [menu_option, 'NL',  '#i. View inventory', '#u. View ship', 'NL', '#Press [ESC] to return'], 24, 0, 0)
        else:
            menu(Map[playerx, playery].planets[planet_num2].name,[menu_option1, menu_option2, 'NL', '#i. View inventory', '#u. View ship', 'NL', '#Press [ESC] to return'], 24, 0, 0)
        #Creating the planet images for first time display
        for z in range(0, num):
            if Map[playerx, playery].planets[z].image is None:
                Map[playerx, playery].planets[z].image = createPlanet(Map[playerx, playery].planets[z].planet_type)
        # looping through planets to see which one is selected, shown by planet_num2
        for i in range(0, num):
            libtcod.image_blit_rect(Map[playerx, playery].planets[i].image, None, 26 + (i * 10), 25, 7, 7,libtcod.BKGND_SET)
            #Displaying the white overlay of selected planet
            if i == planet_num2:
                libtcod.image_blit_rect(selectPlanet(), None, 26 + (i * 10), 25, 7, 7, libtcod.BKGND_SET)
        # print the messages
        displayMessages()
        # show the panels
        displayPanels()
        # blit the contents of "panel" to the root console
        libtcod.console_blit(panel, 0, 0, SCREEN_WIDTH, 3, 0, 0, SCREEN_HEIGHT - 3)
        libtcod.console_flush()
        try:
            exit, planet_num2 = handle_keys()
        except TypeError:
            exit = False
    libtcod.console_clear(None)

def displayMap():
    for row in range(0, SCREEN_HEIGHT - 3):
        for col in range(26, SCREEN_WIDTH - 24):
            if Map[playerx - int(SCREEN_WIDTH / 2) + col, playery - int(SCREEN_HEIGHT / 2) + row].symbol == ".":
                libtcod.console_set_default_foreground(0, libtcod.white)
            else:
                libtcod.console_set_default_foreground(None, libtcod.light_yellow)
            libtcod.console_put_char(0, col, row, Map[playerx - int(SCREEN_WIDTH / 2) + col, playery - int(SCREEN_HEIGHT / 2) + row].symbol, libtcod.BKGND_NONE)
            # libtcod.console_put_char(0, col, row, Map[col, row].symbol, libtcod.BKGND_NONE)
    libtcod.console_set_default_foreground(0, libtcod.white)
    libtcod.console_put_char(0, int(SCREEN_WIDTH / 2), int(SCREEN_HEIGHT / 2), playersymbol, libtcod.BKGND_NONE)

def displayMessages():
    y = 0
    for i in range(0, 67):
        libtcod.console_print_ex(None, 0, i, libtcod.BKGND_NONE, libtcod.LEFT, "                         ")
    for (line, color) in game_msgs:
        libtcod.console_set_default_foreground(None, color)
        if line == "new_line":
            libtcod.console_print_ex(None, 0, y, libtcod.BKGND_NONE, libtcod.LEFT, "                         ")
        else:
            libtcod.console_print_ex(None, 0, y, libtcod.BKGND_NONE, libtcod.LEFT, line)
        y += 1
    libtcod.console_set_default_foreground(0, libtcod.white)

def displayPanels():
    libtcod.console_set_default_background(panel, libtcod.black)
    libtcod.console_clear(panel)
    # The bars to be rendered
    render_bar(panel, 26, 0, 70, 'HULL', player.ship.hull, player.ship.max_hull, libtcod.light_red, libtcod.darker_red)
    render_bar(panel, 26, 1, 70, 'SHIELD', player.ship.shield, player.ship.max_shield, libtcod.light_blue, libtcod.darker_blue)
    render_bar(panel, 26, 2, 70, 'FUEL', player.ship.fuel, player.ship.max_fuel, libtcod.dark_orange, libtcod.darker_orange)

def displayText(start_x, start_y, text_list):
    text_counter = 0
    for text in text_list:
        for i in range(0, len(text)):
            libtcod.console_put_char(0, start_x + i, start_y + text_counter, text[i], libtcod.BKGND_NONE)
        text_counter += 1
    libtcod.console_flush()

def clearMapDisplay():
    for row in range(0, SCREEN_HEIGHT - 3):
        for col in range(26, SCREEN_WIDTH - 24):
            libtcod.console_put_char(0, col, row, " ", libtcod.BKGND_NONE)

def GenerateMap(currentX, currentY, direction):
    # 0 = UP, 1 = DOWN, 2 = LEFT, 3 = RIGHT
    if direction == 0:
        for x in range(currentX - X_Bound - 1, currentX + X_Bound + 1):
            try:
                if Map[x, currentY - Y_Bound - 1] is None:
                    print("loaded")
            except Exception as e:
                r = random.randint(1, 101)
                if (r % 100 == 0):
                    Map[x, currentY - Y_Bound - 1] = MapClass(x, currentY - Y_Bound - 1, "*")
                else:
                    Map[x, currentY - Y_Bound - 1] = MapClass(x, currentY - Y_Bound - 1, " ")
    if direction == 1:
        for x in range(currentX - X_Bound - 1, currentX + X_Bound + 1):
            try:
                if Map[x, currentY + Y_Bound] is None:
                    print("loaded")
            except Exception as e:
                r = random.randint(1, 101)
                if (r % 100 == 0):
                    Map[x, currentY + Y_Bound] = MapClass(x, currentY + Y_Bound, "*")
                else:
                    Map[x, currentY + Y_Bound] = MapClass(x, currentY + Y_Bound, " ")
    if direction == 2:
        for y in range(currentY - Y_Bound - 1, currentY + Y_Bound + 1):
            try:
                if Map[currentX - X_Bound - 1, y] is None:
                    print("loaded")
            except Exception as e:
                r = random.randint(1, 101)
                if (r % 100 == 0):
                    Map[currentX - X_Bound - 1, y] = MapClass(currentX - X_Bound - 1, y, "*")
                else:
                    Map[currentX - X_Bound - 1, y] = MapClass(currentX - X_Bound - 1, y, " ")
    if direction == 3:
        for y in range(currentY - Y_Bound - 1, currentY + Y_Bound + 1):
            try:
                if Map[currentX + X_Bound, y] is None:
                    print("loaded")
            except Exception as e:
                r = random.randint(1, 101)
                if (r % 100 == 0):
                    Map[currentX + X_Bound, y] = MapClass(currentX + X_Bound, y, "*")
                else:
                    Map[currentX + X_Bound, y] = MapClass(currentX + X_Bound, y, " ")


def GenerateInitialMap(currentX, currentY):
    # 0 = UP, 1 = DOWN, 2 = LEFT, 3 = RIGHT
    for x in range(currentX - X_Bound - 1, currentX + X_Bound + 1):
        for y in range(currentY - Y_Bound - 1, currentY + Y_Bound + 1):
            try:
                if Map[x, y] is None:
                    print("loaded")
            except Exception as e:
                r = random.randint(1, 101)
                if (r % 100 == 0):
                    Map[x, y] = MapClass(x, y, "*")
                elif (r % 100 == 1):
                    Map[x, y] = MapClass(x, y, ".")
                else:
                    Map[x, y] = MapClass(x, y, " ")
    Map[playerx, playery] = MapClass(x, y, "*")
    #Create the boundaries surrounding the edges of the map
    createBoundaries()

def createBoundaries():
    # Boundaries for outside the square map
    X_LowerBound = int(((SCREEN_WIDTH - 50) / 2) - 1)
    X_UpperBound = int(Map_X - ((SCREEN_WIDTH - 50) / 2))
    Y_LowerBound = int((SCREEN_HEIGHT - 3) / 2)
    Y_UpperBound = int((Map_Y - ((SCREEN_HEIGHT - 3) / 2)) + 1)
    for i in range(Y_LowerBound, Y_UpperBound):
        Map[X_LowerBound, i] = MapClass(X_LowerBound, i, "|")
        Map[X_UpperBound, i] = MapClass(X_UpperBound, i, "|")
    for j in range(X_LowerBound, X_UpperBound):
        Map[j, Y_LowerBound] = MapClass(j, Y_LowerBound, "-")
        Map[j, Y_UpperBound] = MapClass(j, Y_UpperBound, "-")

def clearScreenAndDelGameMsg():
    del game_msgs[:]
    libtcod.console_clear(None)

def main():
    # Setting custom font and settng up screen
    libtcod.console_set_custom_font('terminal16x16_gs_ro.png', libtcod.FONT_LAYOUT_ASCII_INROW, nb_char_horiz=0, nb_char_vertic=0)
    libtcod.console_init_root(SCREEN_WIDTH, SCREEN_HEIGHT, 'SpaceSim', True, 0)
    libtcod.console_set_default_foreground(0, libtcod.white)
    # Placing initial player character
    libtcod.console_put_char(0, playerx, playery, playersymbol, libtcod.BKGND_NONE)
    libtcod.console_flush()
    # Loading screen
    message_list = ["Loading..."]
    displayText(int(SCREEN_WIDTH / 2 - 9), int(SCREEN_HEIGHT / 2 - 3), message_list)
    libtcod.console_flush()
    # Running map creation and map display
    GenerateInitialMap(playerx, playery)
    # Displaying the map
    displayMainNavigation()

main()
